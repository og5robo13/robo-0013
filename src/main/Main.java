package main;

import robot.Robot;
import main.tasks.*;

public class Main {

	public static Robot robi = new Robot();

	public static void main(String[] args) {
		robi.schedule(new FindSock());
		robi.run();
	}

}
