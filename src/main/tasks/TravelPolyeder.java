package main.tasks;

import robot.Robot;
import robot.RobotTask;

public class TravelPolyeder implements RobotTask {

	private int cornersToTravelTo;
	private int length;
	private double angle;

	@Override
	public void run(Robot robot) {
		robot.go(50.0, this.length);
		robot.rotate(this.angle);
		this.cornersToTravelTo--;
		if (this.cornersToTravelTo >= 1)
			this.run(robot);
		return;
	}

	public TravelPolyeder(int corners, int length) {
		this.cornersToTravelTo = corners;
		this.length = length;
		this.angle = 92;
	}
}
