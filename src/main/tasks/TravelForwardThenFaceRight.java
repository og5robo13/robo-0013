package main.tasks;

import robot.Robot;
import robot.RobotTask;

public class TravelForwardThenFaceRight implements RobotTask {
	
	@Override
	public void run(Robot robot) {
		robot.go(100.0, 1500);
		robot.rotate(-90.0);
		return;
	}

}
