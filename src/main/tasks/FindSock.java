package main.tasks;

import java.util.Random;

import lejos.nxt.LightSensor;
import lejos.nxt.Sound;
import robot.MyLightSensor;
import robot.MyTouchSensor;
import robot.Robot;
import robot.RobotTask;

public class FindSock implements RobotTask {

	@Override
	public void run(Robot robot) throws InterruptedException {
		MyLightSensor light = robot.getLightSensor();
		MyTouchSensor leftTouch = robot.getLeftTouchSensor();
		MyTouchSensor rightTouch = robot.getRightTouchSensor();

		robot.vorwaerts(12000.0);
		while (true) {
			if (isSockVisible(light)) {
				break;
			}
			if (isTouched(leftTouch, rightTouch)) {
				robot.vorwaerts(12000.0, -0.03);
				Random rand = new Random();
				robot.rotate(rand.nextInt(720) - 360);
				robot.vorwaerts(12000.0);
			}
		}
		Sound.beepSequenceUp();
		robot.stopp();
	}

	private boolean isSockVisible(LightSensor... lights) {
		for (LightSensor light: lights) {
			if (light.readNormalizedValue() < 256) return true;
		}
		return false;
	}

	private boolean isTouched(MyTouchSensor... touches) {
		for (MyTouchSensor touch: touches) {
			if (touch.isPressed()) return true;
		}
		return false;
	}

}
