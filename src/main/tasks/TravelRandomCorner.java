package main.tasks;

import java.util.Random;

import robot.Robot;
import robot.RobotTask;

public class TravelRandomCorner implements RobotTask {

	private int times = 1;

	@Override
	public void run(Robot robot) {
		Random rand = new Random();
		robot.vorwaerts(100.0, 1.0);
		for (int i = 0; i < this.times; i++) {
			// theoretical 50:50 chance of turning right or left
			if (rand.nextBoolean()) {
				robot.rotate(-90.0);
			} else {
				robot.rotate(90.0);
			}
			robot.vorwaerts(100.0, 1.0);
		}
		return;
	}

	public int getTimes() {
		return times;
	}

	public void setTimes(int times) {
		this.times = times;
	}

	public TravelRandomCorner(int times) {
		this.times = times;
	}

	public TravelRandomCorner() {}

}
