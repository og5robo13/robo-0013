package main.tasks;

import lejos.nxt.LightSensor;
import robot.Robot;
import robot.RobotTask;

public class FollowTheLine implements RobotTask {

	static final double STANDARDLINEWIDTH = 19.0;
	private double speedModifier = 1.0;

	@Override
	public void run(Robot robot) throws InterruptedException {
		LightSensor light = robot.getLightSensor();
		light.calibrateLow();
		double rotationAngle;
		
		// move towards line until the Sensor sees it
		robot.vorwaerts(3.0 * this.speedModifier);
		while (this.isLineVisible(robot.getLightSensor())) {
		}
		
		// move onto line
		robot.vorwaerts(6.0, Robot.LENGTH / 2.0);
		
		// rotate until Sensor sees Line
		rotationAngle = this.rotateUntilLineVisible(robot, true);
		
		while (true) {
			robot.vorwaerts(3.0 * this.speedModifier);
			while (!(this.isLineVisible(robot.getLightSensor()))) {
			}
			// wenn mehr als 90 Grad gedreht, ist Linie links von Sensor
			if (rotationAngle < 30.0) {
				rotationAngle = this.rotateUntilLineVisible(robot, true);
			// wenn weniger, ist Linie rechts von Sensor
			} else {
				rotationAngle = this.rotateUntilLineVisible(robot, false);
			}
		}

	}

	private boolean isLineVisible(LightSensor light) {
		return light.readNormalizedValue() < light.getLow() * 1.25;
	}

	public double rotateUntilLineVisible(Robot robot, final boolean left) {
		if (left) robot.rotateLeft(); else robot.rotateRight();
		while (this.isLineVisible(robot.getLightSensor())) {
		}
		return (robot.getAngleIncrement() / Robot.ANGLECORRECTION);
	}

	/**
	 * @param lineWidth
	 *            the width of the line in millimeters
	 */
	public FollowTheLine(double lineWidth) {
		this.speedModifier = lineWidth / STANDARDLINEWIDTH;
	}

	public FollowTheLine() {}

}
