package main.tasks;

import robot.Robot;
import robot.RobotTask;

public class TravelInSquare implements RobotTask {

	private int length;
	
	@Override
	public void run(Robot robot) throws InterruptedException {
		RobotTask travelPolyeder = new TravelPolyeder(4, this.length);
		travelPolyeder.run(robot);
		return;
	}
	
	public TravelInSquare(int length) {
		this.length = length;
	}

}
