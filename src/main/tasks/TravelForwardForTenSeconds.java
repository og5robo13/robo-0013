package main.tasks;

import robot.Robot;
import robot.RobotTask;

public class TravelForwardForTenSeconds implements RobotTask {

	@Override
	public void run(Robot robot) {
		robot.go(10000);
		return;
	}

}
