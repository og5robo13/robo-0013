package main.tasks;

import robot.Robot;
import robot.RobotTask;

public class TravelInCircle implements RobotTask {

	private double radius;
	
	@Override
	public void run(Robot robot) {
		robot.arc(this.radius, 380.0);
		return;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public TravelInCircle(double radius) {
		this.radius = radius;
	}

}
