package main.tasks;

import robot.Robot;
import robot.RobotTask;

public class TravelForward implements RobotTask {

	@Override
	public void run(Robot robot) {
		robot.go(1500);
		return;
	}

}
