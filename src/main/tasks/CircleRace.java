package main.tasks;

import lejos.nxt.LightSensor;
import robot.Robot;
import robot.RobotTask;

public class CircleRace implements RobotTask {

	static final double STANDARDLINEWIDTH = 19.0;
	private double speedModifier = 1.6;

	@Override
	public void run(Robot robot) throws InterruptedException {
		LightSensor light = robot.getLightSensor();
		light.calibrateLow();
		boolean clockwiseIsSet = false;
		boolean clockwise = true;
		robot.setRotateSpeed(100.0);
		int timesTurnedLeft = 0;
		int timesTurnedRight = 0;

		while (true) {
			robot.vorwaerts(3.0 * this.speedModifier);
			while (true) {
				if (!this.isLineVisible(robot.getLightSensor()))
					break;
			}

			boolean cancel = false;

			if (timesTurnedRight >= timesTurnedLeft) {
				// testen, ob Linie rechts ist
				robot.rotateRight();

				while (true) {
					if (this.isLineVisible(robot.getLightSensor())) {
						robot.rotate(0.7);
						timesTurnedRight++;
						cancel = true;
						break;
					}
					if (robot.getAngleIncrement() < -45.0) {
						break;
					}
				}
				if (cancel)
					continue;
				// testen, ob Linie links ist
				robot.rotateLeft();
				while (true) {
					if (this.isLineVisible(robot.getLightSensor())) {
						robot.rotate(-0.7);
						cancel = true;
						timesTurnedLeft++;
						break;
					}
					if (robot.getAngleIncrement() > 90.0) {
						break;
					}
				}
				if (cancel)
					continue;
				// zur�ck auf Linie lenken
				robot.rotateLeft();
				while (true) {
					if (this.isLineVisible(robot.getLightSensor())) {
						robot.rotate(0.7);
						cancel = true;
						break;
					}
					if (robot.getAngleIncrement() < 45.0) {
						break;
					}
				}
				if (cancel)
					continue;
			} else {
				// testen, ob Linie links ist
				robot.rotateLeft();
				while (true) {
					if (this.isLineVisible(robot.getLightSensor())) {
						robot.rotate(-0.7);
						cancel = true;
						timesTurnedLeft++;
						break;
					}
					if (robot.getAngleIncrement() > 45.0) {
						break;
					}
				}
				if (cancel)
					continue;
				// testen, ob Linie rechts ist
				robot.rotateRight();

				while (true) {
					if (this.isLineVisible(robot.getLightSensor())) {
						robot.rotate(0.7);
						timesTurnedRight++;
						cancel = true;
						break;
					}
					if (robot.getAngleIncrement() < -90.0) {
						break;
					}
				}
				if (cancel)
					continue;
				// zur�ck auf Linie lenken
				robot.rotateLeft();
				while (true) {
					if (this.isLineVisible(robot.getLightSensor())) {
						robot.rotate(-0.7);
						cancel = true;
						break;
					}
					if (robot.getAngleIncrement() < 45.0) {
						break;
					}
				}
				if (cancel)
					continue;
			}

			double angleIncrement = robot.getAngleIncrement() / Robot.ANGLECORRECTION;

			if (!clockwiseIsSet) {
				// testen, ob Linie rechts ist
				robot.rotateRight();
				while (true) {
					if (this.isLineVisible(robot.getLightSensor())) {
						clockwise = true;
						clockwiseIsSet = true;
						cancel = true;
						break;
					}
					if (robot.getAngleIncrement() - angleIncrement >= 90.0) {
						break;
					}
				}
				if (cancel)
					continue;

				// testen, ob Linie links ist
				robot.quickStop();
				robot.rotateLeft();
				while (true) {
					if (this.isLineVisible(robot.getLightSensor())) {
						clockwise = false;
						clockwiseIsSet = true;
						cancel = true;
						break;
					}
					if (robot.getAngleIncrement() - angleIncrement <= 90.0) {
						break;
					}
				}
				if (cancel)
					continue;
				return;
			} else {
				if (clockwise) {
					robot.arcForward(-15.0);
					while (true) {
						if (this.isLineVisible(robot.getLightSensor())) {
							break;
						}
					}
				} else {
					robot.arcForward(15.0);
					while (true) {
						if (this.isLineVisible(robot.getLightSensor()))
							break;
					}
				}
			}
		}

	}

	private boolean isLineVisible(LightSensor light) {
		return light.readNormalizedValue() < light.getLow() * 1.25;
	}

}
