package main.tasks;

import java.util.Random;

import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import robot.Robot;
import robot.RobotTask;

public class StayOnTable implements RobotTask {

	private class TableEdge extends Exception {

	}

	private class MoveLightSensor implements Runnable {

		@Override
		public void run() {
			Motor.B.setSpeed(90);
			Motor.B.rotate(45);
			while (true) {
				Motor.B.rotate(-90);
				Motor.B.rotate(90);
			}
		}

	}

	private double speedModifier = 1.0;

	@Override
	public void run(Robot robot) throws InterruptedException {
		LightSensor light = robot.getLightSensor();
		light.calibrateHigh();
		Thread moveLightSensor = new Thread(new MoveLightSensor());
		moveLightSensor.start();

		while (true) {
			// move forwards until the sensor doesn't see the table anymore
			robot.vorwaerts(2500.0 * this.speedModifier);
			while (isTableVisible(light)) {
			}
			robot.go(300, true);
			Random rand = new Random();
			robot.rotate(rand.nextInt(720) - 360);
		}

	}

	private boolean isTableVisible(LightSensor light) {
		return light.readNormalizedValue() > light.getHigh() * 0.75;
	}

}
