package main.tasks;

import robot.Robot;
import robot.RobotTask;

public class TravelOneMeter implements RobotTask {
	
	@Override
	public void run(Robot robot) {
		robot.vorwaerts(50.0, 1.0);
		return;
	}
	
}
