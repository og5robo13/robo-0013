package robot;

public interface FahrzeugInterface {

	void fahreVorwaertsStrecke(int i);

	void dreheRechtsGrad(int i);

	void setGeschwindigkeit(int i, int j);

	//void setGeschwindigkeit(int i, int j);

}
