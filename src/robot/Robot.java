/**
 * A representation of a physical Lego NXT robot
 */

package robot;

import java.util.LinkedList;

import lejos.nxt.SensorPort;
import lejos.nxt.Button;
import lejos.nxt.Motor;
import lejos.nxt.Sound;
import lejos.robotics.navigation.DifferentialPilot;
import main.tasks.PlayMario;

public class Robot extends DifferentialPilot implements RobotTaskExecutor {

	/**
	 * Diameter of the wheels of the Robot, in millimeters
	 */
	public static final double WHEELDIAMETER = 56.0;
	/**
	 * Distance between center of left tire and center of right tire, in millimeters
	 */
	public static final double TRACKWIDTH = 113.0;
	/**
	 * Length of the entire Robot
	 */
	public static final double LENGTH = 219.0;
	
	public static final double DISTANCECORRECTION = 1.03;
	
	public static final double ANGLECORRECTION = 1.025;
	/**
	 * The LightSensor of the Robot
	 */
	private MyLightSensor lightSensor = new MyLightSensor(SensorPort.S1);
	private MyTouchSensor rightTouchSensor = new MyTouchSensor(SensorPort.S2);
	private MyTouchSensor leftTouchSensor = new MyTouchSensor(SensorPort.S4);
	// private MyUltrasonicSensor ultrasonicSensor = new MyUltrasonicSensor(SensorPort.S3);

	private LinkedList<RobotTask> tasks = new LinkedList<RobotTask>();
	private void playMusic() {
		try {
			new PlayMario().run(this);
		} catch (InterruptedException ignored) {
		}
	}
	private Thread musicThread = new Thread(new Runnable() {
		public void run() {
			playMusic();
		}
	});

	/**
	 * Schedule RobotTasks to be executed when this.run is called
	 * 
	 * @param robotTasks
	 *            RobotTasks to be scheduled
	 */
	public void schedule(RobotTask... robotTasks) {
		for (RobotTask task : robotTasks) {
			this.tasks.add(task);
		}
	}

	/**
	 * Schedule an Iterable of RobotTasks to be executed when this.run is called
	 * 
	 * @param robotTasks
	 *            An Iterable of RobotTasks to be scheduled
	 */
	public void schedule(Iterable<RobotTask> robotTasks) {
		for (RobotTask task : robotTasks) {
			this.tasks.add(task);
		}
	}

	/**
	 * Run scheduled RobotTasks
	 */
	public void run() {
		// execute the tasks in the order they were scheduled
		try {
			runTasks(this, this.tasks);
		} catch (InterruptedException silenced) {} finally {
			Sound.beepSequenceUp(); // finished
		}
	}

	/**
	 * Execute an Iterable of RobotTasks recursively
	 * 
	 * @param robot
	 *            The Robot instance to run the RobotTask with
	 * @param robotTasks
	 *            The RobotTask to run
	 * @throws InterruptedException 
	 */
	public static void runTasks(Robot robot, LinkedList<RobotTask> robotTasks) throws InterruptedException {
		Sound.twoBeeps(); // ready to start task
		// Every task has to be started manually for easier testing
		for (RobotTask task: robotTasks) {
			try {
				Button.ENTER.waitForPressAndRelease();
				if (!robot.musicThread.isAlive()) robot.musicThread.start();
				task.run(robot);
			} catch (InterruptedException silenced) {
			}
		}
		if (robot.musicThread.isAlive()) robot.musicThread.interrupt();
	}

	/**
	 * Make the Robot move forward or backward (sane alternative to robot.travel)
	 * 
	 * @param speed
	 *            speed in millimeters per second; maximum speed is 440mm/s
	 * @param distance
	 *            distance in meters
	 */
	public void vorwaerts(final double speed, final double distance) {
		this.setTravelSpeed(speed * WHEELDIAMETER);
		this.travel(distance * 1000 * DISTANCECORRECTION);
	}

	public void stopp() {
		this.stop();
	}

	/**
	 * Make the Robot move forward or backward (sane alternative to robot.travel)
	 * 
	 * @param time
	 *            time in milliseconds
	 */
	public void go(final int time) {
		this.go(time, false);
	}

	/**
	 * Make the Robot move forward or backward (sane alternative to robot.travel)
	 * 
	 * @param speed
	 *            speed in millimeters per second; maximum speed is 440mm/s
	 * @param backward
	 *            if true, move backward instead of forward
	 */
	public void go(final int time, boolean backward) {
		if (backward) {
			this._left.backward();
			this._right.backward();
		} else {
			this._left.forward();
			this._right.forward();
		}
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
		}
		this._left.stop(true);
		this._right.stop();
	}

	/**
	 * Make the Robot move forward or backward (sane alternative to robot.travel)
	 * 
	 * @param speed
	 *            speed in millimeters per second; maximum speed is 440mm/s
	 * @param time
	 *            time in milliseconds
	 */
	public void go(final double speed, final int time) {
		this.setTravelSpeed(speed * WHEELDIAMETER);

		if (speed >= 0) {
			this._left.forward();
			this._right.forward();
		} else {
			this._left.backward();
			this._right.backward();
		}
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
		}
		this._left.stop(true);
		this._right.stop();
	}

	/**
	 * Make the Robot move forward or backward (sane alternative to robot.travel)
	 * 
	 * @param speed
	 *            speed in millimeters per second; maximum speed is 440mm/s
	 */
	public void vorwaerts(final double speed) {
		this.setTravelSpeed(speed / WHEELDIAMETER);
		if (speed >= 0)
			this.forward();
		else
			this.backward();
	}

	public void rotate(final double angle) {
		super.rotate(angle * ANGLECORRECTION);
	}

	public MyLightSensor getLightSensor() {
		return lightSensor;
	}

	public MyTouchSensor getRightTouchSensor() {
		return rightTouchSensor;
	}

	public MyTouchSensor getLeftTouchSensor() {
		return leftTouchSensor;
	}

	public Robot() {
		super(WHEELDIAMETER, TRACKWIDTH, Motor.C, Motor.A);
		this.setRotateSpeed(120.0);
	}

}
