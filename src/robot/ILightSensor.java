package robot;

public interface ILightSensor {
	boolean isDark();
	boolean isLight();
}
