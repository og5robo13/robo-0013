package robot;

public interface RobotTaskExecutor {

	/**
	 * Schedule RobotTasks to be executed when run is called
	 * 
	 * @param robotTasks
	 *            RobotTasks to be scheduled
	 */
	public void schedule(RobotTask... robotTasks);

	/**
	 * Schedule an Iterable of RobotTasks to be executed when run is called
	 * 
	 * @param robotTasks
	 *            An Iterable of RobotTasks to be scheduled
	 */
	public void schedule(Iterable<RobotTask> robotTasks);

	/**
	 * Run the scheduled RobotTasks
	 */
	public void run();

}
