package robot;

import lejos.nxt.ADSensorPort;
import lejos.nxt.LightSensor;

public class MyLightSensor extends LightSensor implements ILightSensor {

	public MyLightSensor(ADSensorPort port) {
		super(port);
	}

	@Override
	public boolean isDark() {
		return this.getNormalizedLightValue() <= 255;
	}

	@Override
	public boolean isLight() {
		return this.getNormalizedLightValue() >= 768;
	}

}
