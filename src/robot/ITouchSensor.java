package robot;

public interface ITouchSensor {

	/*
	 * Checks if the TouchSensor is triggered
	 * @returns
	 *     Whether the TouchSensor is triggered
	 */
	boolean isPressed();

}
