package robot;

public interface RobotTask {
	
	public void run(Robot robot) throws InterruptedException;
	
}
